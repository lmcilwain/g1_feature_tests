=begin
Given a valid user
And goes to the documentation section for a class
And selects a child
And sets an observation date within the winter checkpoint period
And a note is added
When the save and review button is clicked
Then the documentation is added to the winter checkpoint period
=end
require 'spec_helper'

CONFIG['winter'].each do |month_name, date|
  feature 'Winter checkpoint', js: true do
    scenario "Documents created in #{month_name} on #{date} fall into the winter checkpoint range" do
      # => Given a valid user
      login_with_username
      # => And click on documentation for a given class
      goto_documentation_for_class
      # => And select a child
      select_child_with_value
      # => And add an observation date
      set_observation_date_to date
      # => And a note is added
      find('#bttnToggle_notes1').click
      find('#dimCheckbox_notes1').set(true)
      # => And click the save and review method
      save_and_review
      seconds_to_pause
      # => Then documentation should be in winter checkpoint
      expect(page).to have_content 'Observation Added to Winter 2015/2016'
    end
  end
end

# 7687
# Need to look into how to fill in ckeditor.
# CKEDITOR.instances.txtNote selects the object but updateElement('some text'); doesn't appear to be working
# fill_in 'cke_contents_txtNote', with: 'some text'
# fill_in_ckeditor 'cke_txtNote', with: Faker::Lorem.paragraphs(1).first
