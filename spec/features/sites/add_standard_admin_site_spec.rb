=begin
Feature: Add a standard admin site
  In order to create a site
  As an standard administrator
  I want to go through G2
  Scenario: A standard admin can create a site
    Given a standard admin visits g2
    And logs in with valid credentials
    And creates a standard site
    When site is searched
    Then site link shows
=end
require 'spec_helper'

feature 'Creating a standard site', js: true do
  scenario 'a standard admin can add a site to G2', :standard do
    visit CONFIG['g2_alpha_url']
    standard_admin_login

    site_name = Faker::Company.name
    create_standard_site site_name

    search_site site_name

    expect(page).to have_link site_name
  end
end
