=begin
Feature: delete a nc admin site
  In order to delete a site
  As an nc administrator
  I want to go through G2
  Scenario: A nc admin can delete a site
    Given a nc admin visits g2
    And logs in with valid nc admin credentials
    And creates a nc site
    When site is deleted
    Then site link does not show
=end
require 'spec_helper'

feature 'Deleting a nc site', js: true do
  scenario 'a nc admin can delete a site to G2', :nc do
    visit CONFIG['g2_alpha_url']
    k3_admin_login

    site_name = Faker::Company.name
    create_nc_site site_name
    seconds_to_pause 2

    search_site site_name

    delete_site site_name

    search_site site_name

    expect(page).to_not have_link site_name
  end
end
