=begin
Feature: delete a standard admin site
  In order to delete a site
  As a standard administrator
  I want to go through G2
  Scenario: A standard admin can delete a site
    Given a standard admin visits g2
    And logs in with valid credentials
    And creates a standard site
    When site is deleted
    Then site link does not show
=end
require 'spec_helper'

feature 'Deleting a standard site', js: true do
  scenario 'a standard admin can delete a site to G2', :standard do
    visit CONFIG['g2_alpha_url']
    standard_admin_login

    site_name = Faker::Company.name
    create_standard_site site_name

    search_site site_name

    delete_site site_name

    search_site site_name

    expect(page).to_not have_link site_name
  end
end
