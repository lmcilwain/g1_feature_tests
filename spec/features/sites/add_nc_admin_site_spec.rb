=begin
Feature: Add a nc admin site
  In order to create a site
  As an nc administrator
  I want to go through G2
  Scenario: A nc admin can create a site
    Given a nc admin visits g2
    And logs in with valid nc admin credentials
    And creates a nc site
    When site is searched
    Then site link shows
=end
require 'spec_helper'

feature 'Creating a NC site', js: true do
  scenario 'a nc admin can add a site to G2', :nc do
    visit CONFIG['g2_alpha_url']
    k3_admin_login

    site_name = Faker::Company.name

    create_nc_site(site_name)
    seconds_to_pause 2

    search_site site_name

    expect(page).to have_link site_name
  end
end
