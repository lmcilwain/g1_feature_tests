def login_with_username(username='789617')
  visit 'https://tsiweb4.teachingstrategies.com/gold/teachers/checkpointDates.cfm'
  fill_in 'username', with: username
  fill_in 'password', with: 'Asdf1234'
  click_button 'Submit'
end

def standard_admin_login
  fill_in 'username', with: CONFIG['admin']['username']
  fill_in 'password', with: CONFIG['admin']['password']
  click_button 'Sign in'
end

def k3_admin_login
  fill_in 'username', with: CONFIG['k3admin']['username']
  fill_in 'password', with: CONFIG['k3admin']['password']
  click_button 'Sign in'
end
