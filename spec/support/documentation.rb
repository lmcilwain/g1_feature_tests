def goto_documentation_for_class(klass='GOLD-7705 Non-Traditional Classroom 1')
  click_link 'Documentation'
  click_link 'Add Documentation'
  click_link klass
end

def select_child_with_value(value='6853725')
  find('span', text: 'Select Children').click
  find("input[type='checkbox'][value='#{value}']").set(true)
  click_button 'Close'
end

def set_observation_date_to(date='09/18/2015')
  fill_in 'notesDateObserved', with: date
  find(".ui-datepicker-trigger").click
end

def save_and_review
  click_button 'Save and Review'
end
