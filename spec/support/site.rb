def create_standard_site(site_name=Faker::Company.name)
  click_link 'Administration'
  click_link 'Add'
  select 'B3 Test Program 1', from: 'programIDs'
  fill_in 'name', with: site_name
  fill_in 'address1', with: Faker::Address.street_address
  fill_in 'city', with: Faker::Address.city
  select 'MD', from: 'state'
  fill_in 'zip', with: Faker::Number.number(5)
  select 'United States', from: 'countryID'
  fill_in 'phone', with: "#{Faker::Number.number(3)}-#{Faker::Number.number(3)}-#{Faker::Number.number(4)}"
  click_button 'Save'
end

def create_nc_site(site_name=Faker::Company.name)
  click_link 'Administration'
  click_link 'Add'
  select 'Default Program', from: 'programIDs'
  fill_in 'name', with: site_name
  fill_in 'address1', with: Faker::Address.street_address
  fill_in 'city', with: Faker::Address.city
  select 'MD', from: 'state'
  fill_in 'zip', with: Faker::Number.number(5)
  select 'United States', from: 'countryID'
  fill_in 'phone', with: "#{Faker::Number.number(3)}-#{Faker::Number.number(3)}-#{Faker::Number.number(4)}"
  click_button 'Save'
end

def search_site(site_name)
  fill_in 'filter_keywords', with: site_name
  click_button 'Filter'
end

def delete_site(site_name)
  click_link site_name
  click_link 'Delete'
  click_button 'Yes'
end
