def snapshot
  save_and_open_page
end

def seconds_to_pause(n=1)
  sleep n
end
